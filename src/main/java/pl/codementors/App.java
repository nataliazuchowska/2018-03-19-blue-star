package pl.codementors;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Random;

/**
 * Hello world!
 *
 */
public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        Group root = new Group();
        Scene scene = new Scene(root, 600, 600, Color.BLACK);

        Polygon polygon = new Polygon(238.196601125, 390.21130326,
                261.803398875, 317.557050459,
                200, 272.654252801,
                276.39320225, 272.654252801,
                300, 200,
                323.60679775, 272.654252801,
                400, 272.654252801,
                338.196601125, 317.557050459,
                361.803398875, 390.21130326,
                300, 345.308505602);
        Text text = new Text("homeworks are awesome");

        text.setX(210);
        text.setY(190);

        polygon.setFill(Color.BLUE);
        text.setStroke(Color.GREEN);


        root.getChildren().add(polygon);
        root.getChildren().add(text);

        for (int i = 0; i < 8; i++) {
            root.getChildren().add(printRectangle(100*i+0, 0));
            root.getChildren().add(printCircle(100 *i +75, 25));

            root.getChildren().add(printRectangle(0, 100*i +0));
            root.getChildren().add(printCircle(25, 100 *i +75));

            root.getChildren().add(printRectangle(100*i+50, 550));
            root.getChildren().add(printCircle(100 *i +125, 575));

            root.getChildren().add(printRectangle(550, 100*i+50));
            root.getChildren().add(printCircle(575, 100 *i +125));

            stage.setScene(scene);
            stage.show();
        }

    }

    static Rectangle printRectangle(int setX, int setY) {
        Group root = new Group();
        Rectangle rectangle = new Rectangle(50, 50);
        Random ran = new Random();
        ran.nextDouble();
        rectangle.setFill(Color.color(ran.nextDouble(), ran.nextDouble(), ran.nextDouble()));
        rectangle.setX(setX);
        rectangle.setY(setY);

        return rectangle;
    }

    static Circle printCircle(int setX, int setY) {
        Group root = new Group();
        Circle circle = new Circle(25);
        Random ran = new Random();
        ran.nextDouble();
        circle.setFill(Color.color(ran.nextDouble(), ran.nextDouble(), ran.nextDouble()));
        circle.setCenterX(setX);
        circle.setCenterY(setY);

        return circle;

    }
}